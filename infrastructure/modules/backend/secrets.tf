resource "google_secret_manager_secret" "config" {
  project   = var.project_name
  secret_id = "${var.name}-config"
  replication {
    auto {}
  }
}

resource "google_secret_manager_secret_iam_member" "member" {
  project   = var.project_name
  secret_id = google_secret_manager_secret.config.secret_id
  role      = "roles/secretmanager.secretAccessor"
  member    = google_service_account.sa.member
}
