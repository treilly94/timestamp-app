resource "google_bigquery_dataset" "dataset" {
  dataset_id = var.env
  location   = var.location
  project    = var.project_name
}

resource "google_bigquery_table" "records" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  table_id   = "records"
  project    = var.project_name

  schema = <<EOF
[
  {
    "name": "ID",
    "type": "STRING",
    "mode": "REQUIRED"
  },
  {
    "name": "Datetime",
    "type": "DATETIME",
    "mode": "REQUIRED"
  },
  {
    "name": "Latitude",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "Longitude",
    "type": "FLOAT",
    "mode": "NULLABLE"
  }
]
EOF

}

resource "google_bigquery_dataset_iam_member" "editor" {
  project    = var.project_name
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  role       = "roles/bigquery.dataEditor"
  member     = google_service_account.sa.member
}
