variable "name" {
  type = string
}

variable "location" {
  type = string
}

variable "project_name" {
  type = string
}

variable "env" {
  type = string
}

variable "deploy_cloudrun" {
  type    = bool
  default = false
}

variable "dns_name" {
  type = string
}
