resource "google_service_account" "sa" {
  project    = var.project_name
  account_id = var.name
}

resource "google_project_iam_member" "datastore" {
  project = var.project_name
  role    = "roles/datastore.user"
  member  = google_service_account.sa.member
}

resource "google_project_iam_member" "bigquery" {
  project = var.project_name
  role    = "roles/bigquery.user"
  member  = google_service_account.sa.member
}
