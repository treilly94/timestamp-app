resource "google_cloud_run_service" "default" {
  count = var.deploy_cloudrun ? 1 : 0

  name     = var.name
  location = var.location
  project  = var.project_name

  metadata {
    annotations = {
      "run.googleapis.com/ingress" = "all"
    }
  }

  template {
    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "2"
      }
    }
    spec {
      service_account_name = google_service_account.sa.email
      containers {
        image = "${var.location}-docker.pkg.dev/timestampapp/timestampapp/backend:${var.env}"
        ports {
          container_port = 8080
        }
        env {
          name  = "GIN_MODE"
          value = "release"
        }
        volume_mounts {
          name       = "config"
          mount_path = "/config"
        }
      }
      volumes {
        name = "config"
        secret {
          secret_name = google_secret_manager_secret.config.secret_id
          items {
            key  = "latest"
            path = "conf.json"
          }
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_member" "noauth" {
  count = var.deploy_cloudrun ? 1 : 0

  location = google_cloud_run_service.default[0].location
  project  = google_cloud_run_service.default[0].project
  service  = google_cloud_run_service.default[0].name

  role   = "roles/run.invoker"
  member = "allUsers"
}
