resource "google_artifact_registry_repository" "docker" {
  project       = var.project_name
  location      = var.location
  repository_id = "timestampapp"
  description   = "docker repository"
  format        = "DOCKER"
  cleanup_policy_dry_run = false
  cleanup_policies {
    id     = "keep-recent"
    action = "KEEP"
    most_recent_versions {
      keep_count            = 5
    }
  }
}
