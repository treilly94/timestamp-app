resource "google_dns_managed_zone" "default" {
  name     = "timestamp-app"
  project  = var.project_name
  dns_name = var.dns_name
}
