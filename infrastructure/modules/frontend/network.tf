resource "google_cloud_run_domain_mapping" "default" {
  count = var.deploy_cloudrun ? 1 : 0

  location = var.location
  project  = var.project_name
  name     = var.dns_name

  metadata {
    namespace = var.project_name
  }

  spec {
    route_name = google_cloud_run_service.default[0].name
  }
}
