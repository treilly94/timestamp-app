resource "google_service_account" "sa" {
  project    = var.project_name
  account_id = var.name
}
