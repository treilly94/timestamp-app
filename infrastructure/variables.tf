variable "project_name" {
  type = string
}

variable "billing_account" {
  type = string
}

variable "location" {
  type = string
}
