terraform {
  backend "http" {}
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0.0"
    }
  }
}

provider "google" {}

module "general" {
  source = "./modules/general"

  project_name    = var.project_name
  billing_account = var.billing_account
  location        = var.location

  dns_name = "timestamp.treilly.co.uk."
}

module "prod_backend" {
  source = "./modules/backend"

  name            = "prod-backend"
  project_name    = var.project_name
  location        = var.location
  env             = "prod"
  dns_name        = "api.timestamp.treilly.co.uk"
  deploy_cloudrun = true

  depends_on = [module.general]
}

module "dev_backend" {
  source = "./modules/backend"

  name            = "dev-backend"
  project_name    = var.project_name
  location        = var.location
  env             = "dev"
  dns_name        = "api.dev.timestamp.treilly.co.uk"
  deploy_cloudrun = false

  depends_on = [module.general]
}

module "prod_frontend" {
  source = "./modules/frontend"

  name            = "prod-frontend"
  project_name    = var.project_name
  location        = var.location
  env             = "prod"
  dns_name        = "timestamp.treilly.co.uk"
  deploy_cloudrun = true

  depends_on = [module.general]
}
