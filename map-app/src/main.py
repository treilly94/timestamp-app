import functions_framework
import folium
from folium import plugins
from google.cloud import bigquery

client = bigquery.Client()


@functions_framework.http
def generate_map(request):
    headers = {"Access-Control-Allow-Origin": "*"}
    query = """
    SELECT Latitude, Longitude
    FROM `timestampapp.prod.records`
    WHERE Latitude is not null;
    """
    query_job = client.query(query)
    rows = query_job.result()

    map = folium.Map()
    heat_data = [[r.Latitude, r.Longitude] for r in rows]
    plugins.HeatMap(heat_data).add_to(map)

    return (map._repr_html_(), 200, headers)
