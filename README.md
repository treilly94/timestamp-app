# Timestamp App

## Backend

The backend App is written in Golang

## Infrastructure

Terraform to setup the cloud infrastructure

## Notebooks

Jupyter notebooks for data exploration
