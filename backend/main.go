package main

import (
	"backend/common"
	"backend/records"
	"backend/routes"
)

func main() {
	common.ReadConfig()
	common.ConnectBigquery()

	records.InitTable()

	routes.Run()
}
