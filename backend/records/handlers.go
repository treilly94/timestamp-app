package records

import (
	"backend/common"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func GetId(c *gin.Context) {
	id := c.Param("id")
	c.IndentedJSON(http.StatusOK, read(ctx, id, 10))
}

func GetAll(c *gin.Context) {
	queryLimit := c.DefaultQuery("limit", "10")
	limit, _ := strconv.Atoi(queryLimit)
	c.IndentedJSON(http.StatusOK, read(ctx, "", limit))
}

func Post(c *gin.Context) {
	var r record
	var rlist []record

	err := c.BindJSON(&r)

	if err != nil {
		common.ProcessError(err)
	}

	r.ID = uuid.NewString()
	rlist = append(rlist, r)

	write(ctx, rlist)
	notify(r)
	c.Status(http.StatusCreated)
}

func PostBulk(c *gin.Context) {
	var records []record

	err := c.BindJSON(&records)

	if err != nil {
		common.ProcessError(err)
	}

	for i := range records {
		records[i].ID = uuid.NewString()
	}

	write(ctx, records)
	c.IndentedJSON(http.StatusCreated, fmt.Sprintf("Uploaded %d records", len(records)))
}

func Delete(c *gin.Context) {
	id := c.Param("id")
	c.IndentedJSON(http.StatusAccepted, fmt.Sprintf("deleted %s", id))
}

func DeleteAll(c *gin.Context) {
	c.IndentedJSON(http.StatusAccepted, "deleted all")
}

func GetStats(c *gin.Context) {
	var data stats
	data.Count = countAll(ctx)
	// data.WeekCount = countBetween(now.AddDate(0, 0, -7), now)
	data.HourCount = fillCount(groupCount(ctx, "HOUR", true), 0, 23)
	data.HourAverage = fillCount(averageCount(ctx, "HOUR"), 0, 23)
	data.DOTWCount = fillCount(groupCount(ctx, "DAYOFWEEK", true), 1, 7)
	data.DOTWAverage = fillCount(averageCount(ctx, "DAYOFWEEK"), 1, 7)
	data.MonthCount = fillCount(groupCount(ctx, "MONTH", true), 1, 12)
	data.MonthAverage = fillCount(averageCount(ctx, "MONTH"), 1, 12)
	data.YearCount = groupCount(ctx, "YEAR", false)

	c.IndentedJSON(http.StatusOK, data)
}
