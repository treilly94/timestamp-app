package records

import (
	"backend/common"
	"context"
	"fmt"
	"slices"
	"sort"

	"cloud.google.com/go/civil"
)

var ctx = context.Background()

type record struct {
	ID        string         `json:"id"`
	Datetime  civil.DateTime `json:"datetime"`
	Latitude  float64        `json:"latitude"`
	Longitude float64        `json:"longitude"`
}

type count struct {
	Part  int
	Count int
}

type stats struct {
	Count        int64   `json:"count"`
	WeekCount    int64   `json:"weekcount"`
	HourCount    []count `json:"hourcount"`
	HourAverage  []count `json:"houraverage"`
	DOTWCount    []count `json:"dotwcount"`
	DOTWAverage  []count `json:"dotwaverage"`
	MonthCount   []count `json:"monthcount"`
	MonthAverage []count `json:"monthaverage"`
	YearCount    []count `json:"yearcount"`
}

func notify(r record) {
	message := fmt.Sprintf(
		"%s\n\nLat: %f\nLong: %f",
		r.Datetime,
		r.Latitude,
		r.Longitude,
	)
	common.Wirepusher("Timestamp Success", message, "Success")
}

func fillCount(data []count, start int, limit int) []count {
	var parts []int
	for _, v := range data {
		parts = append(parts, v.Part)
	}
	for i := start; i <= limit; i++ {
		if !slices.Contains(parts, i) {
			data = append(data, count{i, 0})
		}
	}
	sort.SliceStable(data, func(i, j int) bool {
		return data[i].Part < data[j].Part
	})
	return data
}
