package records

import (
	"backend/common"
	"context"
	"fmt"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/iterator"
)

var table *bigquery.Table
var tableId string

func InitTable() {
	table = common.Dataset.Table("records")
	tId, err := table.Identifier(bigquery.StandardSQLID)
	tableId = tId
	if err != nil {
		common.ProcessError(err)
	}
}

func write(ctx context.Context, items []record) {
	u := table.Inserter()

	if err := u.Put(ctx, items); err != nil {
		common.ProcessError(err)
	}
}

func read(ctx context.Context, id string, limit int) []record {
	sql := fmt.Sprintf(`SELECT * FROM %s `, tableId)
	if id != "" {
		sql += `WHERE ID = @id `
	}
	sql += `ORDER BY Datetime desc LIMIT @limit`

	q := common.Client.Query(sql)
	q.Parameters = []bigquery.QueryParameter{
		{Name: "id", Value: id},
		{Name: "limit", Value: limit},
	}

	it, err := q.Read(ctx)
	if err != nil {
		common.ProcessError(err)
	}

	var rlist []record

	for {
		var i record
		err := it.Next(&i)
		if err == iterator.Done {
			break
		}
		if err != nil {
			common.ProcessError(err)
		}
		rlist = append(rlist, i)
	}

	return rlist
}

func groupCount(ctx context.Context, part string, currentYear bool) []count {
	sql := fmt.Sprintf(
		`SELECT EXTRACT(%[2]s from Datetime) as part, count(Datetime) as count FROM %[1]s `,
		tableId, part)

	if currentYear {
		sql += `where EXTRACT(YEAR from Datetime) = EXTRACT(YEAR from CURRENT_TIMESTAMP) `
	}

	sql += `group by part order by part`

	q := common.Client.Query(sql)
	q.Parameters = []bigquery.QueryParameter{
		{Name: "part", Value: part},
	}

	it, err := q.Read(ctx)
	if err != nil {
		common.ProcessError(err)
	}

	var glist []count

	for {
		var i count
		err := it.Next(&i)
		if err == iterator.Done {
			break
		}
		if err != nil {
			common.ProcessError(err)
		}
		glist = append(glist, i)
	}

	return glist
}

func averageCount(ctx context.Context, part string) []count {
	sql := fmt.Sprintf(`
    with q1 as (
        SELECT EXTRACT(YEAR from Datetime) as year, EXTRACT(%[2]s from Datetime) as part, count(Datetime) as count
        FROM %[1]s
        group by year, part
    )

    select part, cast(round(avg(count)) as INT64) as count
    from q1
    group by part
    order by part
    `, tableId, part)

	q := common.Client.Query(sql)
	q.Parameters = []bigquery.QueryParameter{
		{Name: "part", Value: part},
	}

	it, err := q.Read(ctx)
	if err != nil {
		common.ProcessError(err)
	}

	var glist []count

	for {
		var i count
		err := it.Next(&i)
		if err == iterator.Done {
			break
		}
		if err != nil {
			common.ProcessError(err)
		}
		glist = append(glist, i)
	}

	return glist
}

func countAll(ctx context.Context) int64 {
	sql := fmt.Sprintf(`
    SELECT count(Datetime) as count
    FROM %s
    `, tableId)

	q := common.Client.Query(sql)

	it, err := q.Read(ctx)
	if err != nil {
		common.ProcessError(err)
	}

	var row count

	err = it.Next(&row)
	if err != nil {
		common.ProcessError(err)
	}

	return int64(row.Count)
}
