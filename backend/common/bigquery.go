package common

import (
	"context"

	"cloud.google.com/go/bigquery"
)

var Client *bigquery.Client
var Dataset *bigquery.Dataset

func ConnectBigquery() {
	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, Conf.ProjectID)
	if err != nil {
		ProcessError(err)
	}

	Client = client
	Dataset = client.Dataset(Conf.DatasetID)
}
