package common

import (
	"net/http"
	"net/url"
)

func Wirepusher(title string, message string, status string) {
	baseURL := "https://wirepusher.com/send"
	v := url.Values{}
	v.Set("id", Conf.WirepusherID)
	v.Set("title", title)
	v.Set("message", message)
	v.Set("type", status)

	requestURL := baseURL + "?" + v.Encode()

	http.Get(requestURL)
}
