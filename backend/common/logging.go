package common

import (
	"log"
)

func ProcessError(err error) {
	log.Fatal(err)
}
