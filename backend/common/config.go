package common

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	ProjectID    string
	DatasetID    string
	WirepusherID string
}

var Conf Configuration

func ReadConfig() {
	file, err := os.Open("./config/conf.json")
	if err != nil {
		ProcessError(err)
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&Conf)
	if err != nil {
		ProcessError(err)
	}
}
