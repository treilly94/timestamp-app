package routes

import (
	"backend/records"

	"github.com/gin-gonic/gin"
)

func addRecordRoutes(router *gin.Engine) {
	rg := router.Group("/record")
	rg.GET("/:id", records.GetId)
	rg.POST("/", records.Post)
	rg.DELETE("/:id", records.Delete)
}

func addRecordsRoutes(router *gin.Engine) {
	rg := router.Group("/records")
	rg.GET("/", records.GetAll)
	rg.POST("/", records.PostBulk)
	rg.DELETE("/", records.DeleteAll)
	rg.GET("/stats", records.GetStats)
}
