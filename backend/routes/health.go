package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func addHealthRoutes(router *gin.Engine) {
	rg := router.Group("/health")

	rg.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, "pong")
	})
}
