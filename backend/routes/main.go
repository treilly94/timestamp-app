package routes

import "github.com/gin-gonic/gin"

var router = gin.Default()

func Run() {
	router.Use(corsMiddleware())

	addHealthRoutes(router)
	addRecordRoutes(router)
	addRecordsRoutes(router)

	router.Run(":8080")
}
