FROM golang:1.21.0 AS build

WORKDIR /app

COPY . .
RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -o /backend

# Deploy the application binary into a lean image
# Cant use scratch because
# https://stackoverflow.com/questions/56102761/issue-with-connecting-golang-application-on-cloud-run-with-firestore
FROM alpine

WORKDIR /

COPY --from=build /backend /backend
RUN apk add --no-cache ca-certificates

EXPOSE 8080

ENTRYPOINT ["/backend"]
